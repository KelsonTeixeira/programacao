#include <stdio.h>
#include <stdlib.h>

troca(int *n, int *k){
	int tmp;

	tmp = *n;
	*n = *k;
	*k = tmp;

}

int main(){
	int n, k;

	scanf("%d %d", &n, &k);

	troca(&n, &k);

	printf("(%d, %d)", n, k);
	
}

