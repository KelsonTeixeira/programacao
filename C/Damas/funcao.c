#include<stdio.h>
#include<stdlib.h>

int x_isdigit(char ch){
	return (ch >= '0' && ch <= '9');
}	

int main(){
	
	char c;

	while(1){ //So para o programa quando apertar ctrl-C
		c = getchar();
		if(!x_isdigit(c)){
			putchar(c);
		}
	}
}