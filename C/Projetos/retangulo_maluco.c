#include<stdio.h>

int main(){

	int dim, i, j;

	scanf("%d", &dim);
	
	for(i = 0; i < dim; i++){
		printf("#");
	}

	printf("\n");

	for(j = 2; j < dim; j++){
		printf("#");
		for(i = 1; i < dim; i++){
			if(i == dim-1){
				printf("#");
			}
			else{
				if(dim%2 == 0){
					if(i == dim - j || i == j - 1){
						printf("#");
					}
					else{
						printf("-");
					}
				}
				else{
					if(i == (dim/2) || j == (dim/2)+1){
						printf("#");
					}
					else{
						printf("-");
					}
				}
			}
		}
		printf("\n");
	}

	for(i = 0; i < dim; i++){
		printf("#");
	}

	printf("\n");

	return 0;
}